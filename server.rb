require "sinatra"
require "json"
require "net/http"

set :public_folder, 'public'

AP_MIME = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"';
AP_HEADERS = {"Content-Type" => AP_MIME, "Accept" => AP_MIME}

REMOTE_HOST = ENV['REMOTE_HOST'] || "127.0.0.1"
REMOTE_PORT = ENV['REMOTE_PORT'] || "3000"

LOCAL_HOST = ENV['LOCAL_HOST'] || "127.0.0.1"
LOCAL_PORT = ENV['LOCAL_PORT'] || "4567"

get '/' do
  @host = REMOTE_HOST
  @port = REMOTE_PORT
  erb :index
end

post '/subscribe' do
  content = JSON.parse(request.body.read)
  uri = URI(content['inboxUrl'])

  activity = {
    '@context': 'https://www.w3.org/ns/activitystreams',
    'id': "http://#{LOCAL_HOST}:#{LOCAL_PORT}/follow-activity-id",
    'type': 'Follow',
    'actor': "http://#{LOCAL_HOST}:#{LOCAL_PORT}/subscriber",
    'object': content['inboxUrl'].sub(/\/inbox$/, ''),
  }

  resp = Net::HTTP.post(uri, activity.to_json, AP_HEADERS)
  resp.body
end

get '/subscriber' do
  JSON.dump({
    '@context': 'https://www.w3.org/ns/activitystreams',
    'id': "http://#{LOCAL_HOST}:#{LOCAL_PORT}/subscriber",
    'inbox': "http://#{LOCAL_HOST}:#{LOCAL_PORT}/subscriber/inbox",
  })
end

post '/subscriber/inbox' do
  puts "REQUEST:"
  p request
  puts "\nPAYLOAD:"
  content = JSON.parse(request.body.read)
  p content
  puts
  JSON.dump({"success": true})
end

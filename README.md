# ActivityPub review

Sinatra app to help testing ActivityPub features for reviews, written for
[this MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132460).

## Install

```
git clone https://gitlab.com/oelmekki/activitypub-review
bundle
```

You need to do a bit of preparation in GitLab:

1. make flightjs/Flight a public project
2. enable the feature flags:

```
Feature.enable(:activity_pub_project)
Feature.enable(:activity_pub)
```

3. Allow local requests in GitLab : Go to admin > settings > network > outbound requests and check "Allow requests to the local network from webhooks and integrations".

Alternatively, you can do all of that from a rails console, assuming
initial state from `rails seed_fu`:

```ruby
Feature.enable(:activity_pub)
Feature.enable(:activity_pub_project)
ApplicationSetting.first.update(allow_local_requests_from_web_hooks_and_services: true)
flight = Project.find_by_name("Flight")
flight.update(visibility: "public")
```

You're ready to go!

## Run

```
ruby server.rb

# if you need to change host/port of GitLab (REMOTE) or of the sinatra app (LOCAL):
REMOTE_HOST=192.168.10.9 REMOTE_PORT=3000 LOCAL_HOST=192.168.10.3 ruby server.rb -o 0.0.0.0
```

Opens [http://localhost:4567/](http://localhost:4567/). Further
instructions will be given for the MR you're checking.


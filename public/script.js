function subscribe() {
  const $container = document.getElementById("subscribe");
  const $url = $container.querySelector("input.inbox-url");
  const $form = $container.querySelector("form");
  const $logs = $container.querySelector(".logs");

  const submit = async inboxUrl => {
    const body = JSON.stringify({inboxUrl});

    const resp = await fetch("/subscribe", { method: 'POST', body });

    const data = await resp.text();
    const $resp = document.createElement("p");
    $resp.textContent = `Response: ${data}`;
    $logs.append($resp);

    const $notice = document.createElement("p");
    $notice.textContent = `Now check sinitra's logs to see GitLab posting an Accept activity!`;
    $logs.append($notice);
  };

  $form.addEventListener("submit", event => {
    event.preventDefault();
    $logs.innerHTML = "";
    $logs.style.display = "";
    submit($url.value);
  });
}

document.addEventListener("DOMContentLoaded", () => {
  subscribe();
});
